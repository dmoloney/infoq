exec { "apt-get update":
  path => "/usr/bin",
}

class rabbitmq::plugins::rabbitmq_management {
        rabbitmq::plugin { 'rabbitmq_management':
                ensure   => present, 
        }
}


class { '::rabbitmq':
  service_manage    => false,
  port              => '5672',
}

class { 'redis':
  version => '2.6.5',
}
